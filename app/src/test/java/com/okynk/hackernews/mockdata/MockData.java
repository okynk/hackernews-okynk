package com.okynk.hackernews.mockdata;


import com.okynk.hackernews.api.model.StoryResponse;
import com.okynk.hackernews.features.storydetail.StoryDetailVM;
import com.okynk.hackernews.features.topstories.StoryVM;

import java.util.ArrayList;
import java.util.List;

public class MockData {

    public static List<Long> getTopStories() {
        List<Long> longs = new ArrayList<>();
        longs.add((long) 15147335);
        longs.add((long) 15145226);
        return longs;
    }

    public static List<Long> getTopStories(int count) {
        List<Long> longs = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            longs.add((long) i);
        }
        return longs;
    }

    public static StoryResponse getStoryResponse_1() {
        StoryResponse storyResponse = new StoryResponse();
        storyResponse.setBy("a");
        storyResponse.setId((long) 1);
        storyResponse.setText("aaaa");
        storyResponse.setTitle("bbbb");
        return storyResponse;
    }

    public static StoryResponse getStoryResponse_15147335() {
        StoryResponse storyResponse = new StoryResponse();
        storyResponse.setBy("15147335");
        storyResponse.setId((long) 15147335);
        storyResponse.setText("15147335");
        storyResponse.setTitle("15147335");
        List<Long> kids = new ArrayList<>();
        kids.add((long) 1);
        storyResponse.setKids(kids);
        return storyResponse;
    }

    public static StoryResponse getStoryResponse_15145226() {
        StoryResponse storyResponse = new StoryResponse();
        storyResponse.setBy("15145226");
        storyResponse.setId((long) 15145226);
        storyResponse.setText("15145226");
        storyResponse.setTitle("15145226");
        List<Long> kids = new ArrayList<>();
        kids.add((long) 1);
        storyResponse.setKids(kids);
        return storyResponse;
    }

    public static StoryVM getStoryVM_15147335() {
        return new StoryVM(getStoryResponse_15147335());
    }

    public static StoryVM getStoryVM_15145226() {
        return new StoryVM(getStoryResponse_15145226());
    }

    public static StoryResponse getStoryResponse(long id) {
        StoryResponse storyResponse = new StoryResponse();
        storyResponse.setBy(String.valueOf(id));
        storyResponse.setId(id);
        storyResponse.setText(String.valueOf(id));
        storyResponse.setTitle(String.valueOf(id));
        return storyResponse;
    }

    public static List<StoryVM> getStories() {
        List<StoryVM> storyVMs = new ArrayList<>();
        storyVMs.add(getStoryVM_15147335());
        storyVMs.add(getStoryVM_15145226());
        return storyVMs;
    }

    public static List<StoryVM> getStories(List<Long> storyIds) {
        List<StoryVM> storyVMs = new ArrayList<>();
        for (Long storyId : storyIds) {
            storyVMs.add(new StoryVM(getStoryResponse(storyId)));
        }
        return storyVMs;
    }

    public static List<StoryDetailVM> getStoryDetail() {
        List<StoryDetailVM> storyDetailVMs = new ArrayList<>();
        storyDetailVMs.add(new StoryDetailVM(getStoryResponse_15147335(), 1));
        storyDetailVMs.add(new StoryDetailVM(getStoryResponse_1(), 2));
        storyDetailVMs.add(new StoryDetailVM(getStoryResponse_15145226(), 1));
        storyDetailVMs.add(new StoryDetailVM(getStoryResponse_1(), 2));
        return storyDetailVMs;
    }

    public static List<StoryDetailVM> getStoryDetail(List<Long> storyIds) {
        List<StoryDetailVM> storyDetailVMs = new ArrayList<>();
        for (Long storyId : storyIds) {
            storyDetailVMs.add(new StoryDetailVM(getStoryResponse(storyId), 1));
        }
        storyDetailVMs.add(new StoryDetailVM(getStoryResponse_15147335(), 1));
        return storyDetailVMs;
    }
}
