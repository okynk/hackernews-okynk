package com.okynk.hackernews.utils;


import java.lang.reflect.Field;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public final class MockUtil {

    public static FormattingUtil.Clock mockClock(final long millisReturned) throws Exception {
        FormattingUtil.Clock clock = mock(FormattingUtil.Clock.class);
        when(clock.getCurrentTimeMillis()).thenReturn(millisReturned);
        return clock;
    }

    public static FormattingUtil.Clock mockFormattingUtilClock(final long millisReturned)
            throws Exception {
        FormattingUtil.Clock clock = mockClock(millisReturned);
        Field field = FormattingUtil.class.getDeclaredField("sClock");
        field.setAccessible(true);
        field.set(null, clock);
        return clock;
    }
}
