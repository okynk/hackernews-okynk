package com.okynk.hackernews.utils;


import android.support.v4.widget.SwipeRefreshLayout;

import com.okynk.hackernews.RobolectricTest;

import junit.framework.Assert;

import org.junit.Test;

import java.util.ArrayList;

import io.reactivex.disposables.Disposable;
import io.reactivex.subscribers.TestSubscriber;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class UtilTest extends RobolectricTest {

    @Test
    public void testSetSwipeRefreshing_true() throws Exception {
        SwipeRefreshLayout swipeRefreshLayout = mock(SwipeRefreshLayout.class);
        Util.setSwipeRefreshing(swipeRefreshLayout, true);

        verify(swipeRefreshLayout, times(1)).setRefreshing(true);
    }

    @Test
    public void testIsCollectionEmpty() throws Exception {
        assertTrue(Util.isCollectionEmpty(null));

        ArrayList<String> collection = new ArrayList<>();
        assertTrue(Util.isCollectionEmpty(collection));

        collection.add("a");
        assertFalse(Util.isCollectionEmpty(collection));
    }

    @Test
    public void testIsStringEmpty() throws Exception {
        assertTrue(Util.isStringEmpty(null));

        assertTrue(Util.isStringEmpty(""));

        assertFalse(Util.isStringEmpty("aaa"));
    }

    @Test
    public void testSafelyDispose() throws Exception {
        Disposable disposable = new TestSubscriber<>();
        Disposable disposable2 = new TestSubscriber<>();

        Util.safelyDispose(disposable, disposable2);

        if (Util.isDisposableActive(disposable)) {
            Assert.fail("disposable still active");
        }

        if (Util.isDisposableActive(disposable2)) {
            Assert.fail("disposable still active");
        }
    }

    @Test
    public void testIsDisposableActive() throws Exception {
        assertFalse(Util.isDisposableActive(null));

        assertTrue(Util.isDisposableActive(new TestSubscriber<>()));

        Disposable disposable = new TestSubscriber<>();
        disposable.dispose();

        assertFalse(Util.isDisposableActive(disposable));
    }
}
