package com.okynk.hackernews.utils;


import android.content.Context;
import android.text.SpannableStringBuilder;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;

import com.okynk.hackernews.RobolectricTest;
import com.okynk.hackernews.TestApp;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

public class FormattingUtilTest extends RobolectricTest {

    private Calendar mCalendar;
    private Context mContext;

    @Before
    public void setUp() throws Exception {
        mCalendar = new GregorianCalendar(TimeZone.getTimeZone("GMT+7"));
        mCalendar.clear();
        // 1 October 2017, 19:01:05
        mCalendar.set(2017, 9, 1, 19, 1, 5);
        MockUtil.mockFormattingUtilClock(mCalendar.getTimeInMillis());

        mContext = Mockito.mock(TestApp.class);
    }

    @Test
    public void testFormatStoryTitle() throws Exception {

        SpannableStringBuilder expectedResult = new SpannableStringBuilder("test");
        Assert.assertEquals(expectedResult.toString(),
                FormattingUtil.formatStoryTitle(mContext, "test", "").toString());
        Assert.assertEquals(expectedResult.toString(),
                FormattingUtil.formatStoryTitle(mContext, "test", null).toString());
    }

    @Test
    public void testFormatToRelativeDate() throws Exception {
        final String dateFormat = "dd MMM yyyy";

        Calendar calendar = new GregorianCalendar();
        calendar.clear();
        // 1 October 2017, 19:01:05
        calendar.set(2017, 9, 1, 19, 1, 5);

        String result = FormattingUtil.formatToRelativeDate(calendar.getTimeInMillis(),
                TimeUnit.MILLISECONDS, dateFormat);
        assertEquals("just now", result);

        calendar.clear();
        // 1 October 2017, 19:01:05
        calendar.set(2017, 9, 1, 19, 1, 5);
        calendar.add(Calendar.SECOND, -90);

        result = FormattingUtil.formatToRelativeDate(calendar.getTimeInMillis(),
                TimeUnit.MILLISECONDS, dateFormat);
        assertEquals("1 minutes ago", result);

        calendar.clear();
        // 1 October 2017, 19:01:05
        calendar.set(2017, 9, 1, 19, 1, 5);
        calendar.add(Calendar.MINUTE, -90);

        result = FormattingUtil.formatToRelativeDate(calendar.getTimeInMillis(),
                TimeUnit.MILLISECONDS, dateFormat);
        assertEquals("2 hours ago", result);

        calendar.clear();
        // 1 October 2017, 19:01:05
        calendar.set(2017, 9, 1, 19, 1, 5);
        calendar.add(Calendar.HOUR, -30);

        result = FormattingUtil.formatToRelativeDate(calendar.getTimeInMillis(),
                TimeUnit.MILLISECONDS, dateFormat);
        assertEquals("2 days ago", result);

        calendar.clear();
        // 1 October 2017, 19:01:05
        calendar.set(2017, 9, 1, 19, 1, 5);
        calendar.add(Calendar.DATE, -90);

        result = FormattingUtil.formatToRelativeDate(calendar.getTimeInMillis(),
                TimeUnit.MILLISECONDS, dateFormat);
        assertEquals("03 Jul 2017", result);
    }

    @Test
    public void testSetFontSize() throws Exception {
        SpannableStringBuilder stringBuilder = new SpannableStringBuilder("test");
        AbsoluteSizeSpan[] styles;

        stringBuilder = FormattingUtil.setFontSize(stringBuilder, 0, 2, 5);
        styles = stringBuilder.getSpans(0, stringBuilder.length(), AbsoluteSizeSpan.class);
        assertEquals(1, styles.length);
        assertEquals(0, stringBuilder.getSpanStart(styles[0]));
        assertEquals(2, stringBuilder.getSpanEnd(styles[0]));
        assertEquals(5, styles[0].getSize());
    }

    @Test
    public void testSetForegroundColor() throws Exception {
        SpannableStringBuilder stringBuilder = new SpannableStringBuilder("test");
        ForegroundColorSpan[] styles;

        stringBuilder = FormattingUtil.setForegroundColor(stringBuilder, 0, 2, 5);
        styles = stringBuilder.getSpans(0, stringBuilder.length(), ForegroundColorSpan.class);
        assertEquals(1, styles.length);
        assertEquals(0, stringBuilder.getSpanStart(styles[0]));
        assertEquals(2, stringBuilder.getSpanEnd(styles[0]));
        assertEquals(5, styles[0].getForegroundColor());
    }

    @Test
    public void testTrimWhiteLines() throws Exception {
        assertEquals("test", FormattingUtil.trimWhiteLines("test\n"));
        assertEquals("test", FormattingUtil.trimWhiteLines("test"));
        assertEquals("test\ntest", FormattingUtil.trimWhiteLines("test\ntest\n"));
    }
}
