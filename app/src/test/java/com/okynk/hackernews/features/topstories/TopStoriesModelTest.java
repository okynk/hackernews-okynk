package com.okynk.hackernews.features.topstories;


import com.okynk.hackernews.RobolectricTest;
import com.okynk.hackernews.api.model.StoryResponse;
import com.okynk.hackernews.mockdata.MockData;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.List;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Observable;
import io.reactivex.subscribers.TestSubscriber;

public class TopStoriesModelTest extends RobolectricTest {

    private TopStoriesContract.Repository mRepository;
    private TopStoriesContract.Model mModel;

    @Before
    public void setUp() throws Exception {
        mRepository = Mockito.mock(TopStoriesRepository.class);

        mModel = new TopStoriesModel(mRepository);
    }

    @Test
    public void testGetTopStories() throws Exception {
        Mockito.when(mRepository.getTopStories()).thenReturn(
                Observable.just(MockData.getTopStories()));

        TestSubscriber<List<Long>> testSubscriber = new TestSubscriber<>();
        mModel.getTopStories().toFlowable(BackpressureStrategy.LATEST).subscribe(testSubscriber);

        testSubscriber.assertValueCount(1);
        testSubscriber.assertValue(MockData.getTopStories());

        testSubscriber.awaitTerminalEvent();
    }

    @Test
    public void testGetStories() throws Exception {
        final List<Long> longs = MockData.getTopStories();
        final StoryResponse response15145226 = MockData.getStoryResponse_15145226();
        final StoryResponse response15147335 = MockData.getStoryResponse_15147335();
        Mockito.when(mRepository.getStory(response15145226.getId())).thenReturn(
                Observable.just(response15145226));
        Mockito.when(mRepository.getStory(response15147335.getId())).thenReturn(
                Observable.just(response15147335));

        TestSubscriber<List<StoryVM>> testSubscriber = new TestSubscriber<>();
        mModel.getStories(longs).toFlowable(BackpressureStrategy.LATEST).subscribe(testSubscriber);

        testSubscriber.assertValueCount(1);
        testSubscriber.assertValue(MockData.getStories());

        testSubscriber.awaitTerminalEvent();
    }
}
