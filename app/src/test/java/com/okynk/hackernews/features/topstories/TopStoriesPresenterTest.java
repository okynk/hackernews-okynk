package com.okynk.hackernews.features.topstories;


import android.content.Context;

import com.okynk.hackernews.TestApp;
import com.okynk.hackernews.features.base.BasePresenterTest;
import com.okynk.hackernews.mockdata.MockData;
import com.okynk.hackernews.utils.Util;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatchers;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.subscribers.TestSubscriber;

import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TopStoriesPresenterTest extends BasePresenterTest {

    private TopStoriesContract.Model mModel;
    private TopStoriesContract.View mView;
    private TopStoriesPresenter mPresenter;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        mModel = mock(TopStoriesContract.Model.class);
        Context context = mock(TestApp.class);
        mView = mock(TopStoriesContract.View.class);

        mPresenter = new TopStoriesPresenter(mModel, context, subscribeOn, observeOn);
        mPresenter.start(mView);
    }

    @Test
    public void testGetData() throws Exception {
        final List<Long> storyIds = MockData.getTopStories();
        final List<StoryVM> stories = MockData.getStories();
        when(mModel.getTopStories()).thenReturn(Observable.just(storyIds));
        when(mModel.getStories(storyIds)).thenReturn(Observable.just(stories));

        mPresenter.getData();

        verify(mView, times(1)).setSwipeRefreshing(true);
        verify(mModel, times(1)).getTopStories();
        verify(mModel, times(1)).getStories(storyIds);
        verify(mView, times(1)).setData(stories);
        verify(mView, times(1)).setSwipeRefreshing(false);
        verify(mView, never()).showSnackBar(anyString());
    }

    @Test
    public void testGetData_error() throws Exception {
        final String errorMessage = "errorMessage";
        final Throwable exception = new Throwable(errorMessage);

        when(mModel.getTopStories()).thenReturn(Observable.<List<Long>> error(exception));

        mPresenter.getData();

        verify(mView, times(1)).setSwipeRefreshing(true);
        verify(mModel, times(1)).getTopStories();
        verify(mView, never()).setData(ArgumentMatchers.<StoryVM> anyList());
        verify(mView, times(1)).setSwipeRefreshing(false);
        verify(mView, times(1)).showSnackBar(errorMessage);
    }

    @Test
    public void testGetData_fetchError() throws Exception {
        final List<Long> storyIds = MockData.getTopStories();
        final String errorMessage = "errorMessage";
        final Throwable exception = new Throwable(errorMessage);
        when(mModel.getTopStories()).thenReturn(Observable.just(storyIds));
        when(mModel.getStories(storyIds)).thenReturn(Observable.<List<StoryVM>> error(exception));

        mPresenter.getData();

        verify(mView, times(1)).setSwipeRefreshing(true);
        verify(mModel, times(1)).getTopStories();
        verify(mModel, times(1)).getStories(storyIds);
        verify(mView, never()).setData(ArgumentMatchers.<StoryVM> anyList());
        verify(mView, times(1)).setSwipeRefreshing(false);
        verify(mView, times(1)).showSnackBar(errorMessage);
    }

    @Test
    public void testGetData_networkNotAvailable() throws Exception {
        final Field field = mPresenter.getClass().getDeclaredField("mNetworkAvailable");
        field.setAccessible(true);
        field.set(mPresenter, false);

        mPresenter.getData();
        verify(mView, never()).setSwipeRefreshing(anyBoolean());
        verify(mModel, never()).getTopStories();
        verify(mModel, never()).getStories(ArgumentMatchers.<Long> anyList());
        verify(mView, never()).setData(ArgumentMatchers.<StoryVM> anyList());
        verify(mView, never()).setSwipeRefreshing(anyBoolean());
        verify(mView, never()).showSnackBar(anyString());
    }

    @Test
    public void testPause() throws Exception {
        final Field field = mPresenter.getClass().getDeclaredField("mNetworkDisposable");
        field.setAccessible(true);

        TestSubscriber subscriber = new TestSubscriber();
        field.set(mPresenter, subscriber);

        mPresenter.pause();

        Disposable disposable = (Disposable) field.get(mPresenter);

        if (Util.isDisposableActive(disposable)) Assert.fail("disposable still active");
    }

    @Test
    public void testGetNextPage_networkAvailable_hasNextPage() throws Exception {
        final List<Long> storyIds = MockData.getTopStories(25);
        final List<Long> sublistStoryIds = storyIds.subList(20, 25);
        final List<StoryVM> stories = MockData.getStories(sublistStoryIds);

        final Field mTopStoriesField = mPresenter.getClass().getDeclaredField("mTopStories");
        mTopStoriesField.setAccessible(true);
        mTopStoriesField.set(mPresenter, storyIds);

        when(mModel.getStories(sublistStoryIds)).thenReturn(Observable.just(stories));

        mPresenter.getNextPage();

        verify(mView, times(1)).setSwipeRefreshing(true);
        verify(mModel, times(1)).getStories(sublistStoryIds);
        verify(mView, times(1)).addData(stories);
        verify(mView, times(1)).setSwipeRefreshing(false);
        verify(mView, never()).showSnackBar(anyString());
    }

    @Test
    public void testGetNextPage_networkAvailable_notHasNextPage() throws Exception {
        final List<Long> storyIds = MockData.getTopStories(10);

        final Field mTopStoriesField = mPresenter.getClass().getDeclaredField("mTopStories");
        mTopStoriesField.setAccessible(true);
        mTopStoriesField.set(mPresenter, storyIds);

        mPresenter.getNextPage();

        verify(mView, never()).setSwipeRefreshing(anyBoolean());
        verify(mModel, never()).getStories(ArgumentMatchers.<Long> anyList());
        verify(mView, never()).addData(ArgumentMatchers.<StoryVM> anyList());
        verify(mView, never()).showSnackBar(anyString());
    }

    @Test
    public void testGetNextPage_networkNotAvailable() throws Exception {
        final Field field = mPresenter.getClass().getDeclaredField("mNetworkAvailable");
        field.setAccessible(true);
        field.set(mPresenter, false);

        mPresenter.getNextPage();

        verify(mView, never()).setSwipeRefreshing(anyBoolean());
        verify(mModel, never()).getStories(ArgumentMatchers.<Long> anyList());
        verify(mView, never()).addData(ArgumentMatchers.<StoryVM> anyList());
        verify(mView, never()).showSnackBar(anyString());
    }

    @Test
    public void testOnScrolled_ifTrue() throws Exception {
        final List<Long> storyIds = MockData.getTopStories(25);
        final List<Long> sublistStoryIds = storyIds.subList(20, 25);
        final List<StoryVM> stories = MockData.getStories(sublistStoryIds);

        final Field mTopStoriesField = mPresenter.getClass().getDeclaredField("mTopStories");
        mTopStoriesField.setAccessible(true);
        mTopStoriesField.set(mPresenter, storyIds);

        when(mModel.getStories(sublistStoryIds)).thenReturn(Observable.just(stories));

        mPresenter.onScrolled(15, 5, 40);

        verify(mView, times(1)).setSwipeRefreshing(true);
        verify(mModel, times(1)).getStories(sublistStoryIds);
        verify(mView, times(1)).addData(stories);
        verify(mView, times(1)).setSwipeRefreshing(false);
        verify(mView, never()).showSnackBar(anyString());
    }

    @Test
    public void testOnScrolled_ifFalse() throws Exception {
        mPresenter.onScrolled(15, 5, 60);

        verify(mView, never()).setSwipeRefreshing(anyBoolean());
        verify(mModel, never()).getStories(ArgumentMatchers.<Long> anyList());
        verify(mView, never()).addData(ArgumentMatchers.<StoryVM> anyList());
        verify(mView, never()).showSnackBar(anyString());
    }

    @Test
    public void testDestroy() throws Exception {
        final List<Field> disposableFields = new ArrayList<>();
        final String[] disposableKeys = new String[]{
                "mNetworkDisposable", "mGetDataDisposable", "mFetchStoriesDisposable"
        };

        Field disposableField;
        for (String subscription : disposableKeys) {
            disposableField = mPresenter.getClass().getDeclaredField(subscription);
            disposableField.setAccessible(true);
            disposableFields.add(disposableField);
        }

        TestSubscriber testSubscriber;
        for (Field field : disposableFields) {
            testSubscriber = new TestSubscriber();
            field.set(mPresenter, testSubscriber);
        }

        mPresenter.destroy();

        Disposable disposable;
        for (Field field : disposableFields) {
            disposable = (Disposable) field.get(mPresenter);
            if (Util.isDisposableActive(disposable)) {
                Assert.fail("disposable still active");
            }
        }
    }
}
