package com.okynk.hackernews.features.base;


import com.okynk.hackernews.RobolectricTest;
import com.okynk.hackernews.api.ApiService;
import com.okynk.hackernews.api.ConstantUrl;
import com.okynk.hackernews.api.MockApiService;

import org.junit.Before;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.mock.BehaviorDelegate;
import retrofit2.mock.MockRetrofit;
import retrofit2.mock.NetworkBehavior;

public abstract class BaseRepositoryTest extends RobolectricTest {

    protected MockApiService mockApiService;

    @Before
    public void setUp() throws Exception {
        final NetworkBehavior behavior = NetworkBehavior.create();
        behavior.setDelay(0, TimeUnit.MILLISECONDS);
        behavior.setVariancePercent(0);
        behavior.setFailurePercent(0);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(ConstantUrl.BASE_API_URL).client(
                new OkHttpClient.Builder().build()).addCallAdapterFactory(
                RxJava2CallAdapterFactory.create()).addConverterFactory(
                GsonConverterFactory.create()).build();

        final MockRetrofit mockRetrofit = new MockRetrofit.Builder(retrofit).networkBehavior(
                behavior).build();
        final BehaviorDelegate<ApiService> delegate = mockRetrofit.create(ApiService.class);

        mockApiService = new MockApiService(delegate);
    }
}
