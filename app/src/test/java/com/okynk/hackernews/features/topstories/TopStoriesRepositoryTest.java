package com.okynk.hackernews.features.topstories;


import com.okynk.hackernews.api.model.StoryResponse;
import com.okynk.hackernews.features.base.BaseRepositoryTest;
import com.okynk.hackernews.mockdata.MockData;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import io.reactivex.BackpressureStrategy;
import io.reactivex.subscribers.TestSubscriber;

import static org.junit.Assert.assertEquals;

public class TopStoriesRepositoryTest extends BaseRepositoryTest {

    private TopStoriesContract.Repository mTopStoriesRepository;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        mTopStoriesRepository = new TopStoriesRepository(mockApiService);
    }

    @Test
    public void testGetTopStories() throws Exception {
        TestSubscriber<List<Long>> testSubscriber = new TestSubscriber<>();
        mTopStoriesRepository.getTopStories().toFlowable(BackpressureStrategy.LATEST).subscribe(
                testSubscriber);

        testSubscriber.assertValueCount(1);

        List<Long> result = testSubscriber.values().get(0);
        assertEquals(2, result.size());
        assertEquals(15147335, (long) result.get(0));
        assertEquals(15145226, (long) result.get(1));

        testSubscriber.awaitTerminalEvent();
    }

    @Test
    public void testGetStory() throws Exception {
        StoryResponse expected = MockData.getStoryResponse_1();

        TestSubscriber<StoryResponse> testSubscriber = new TestSubscriber<>();
        mTopStoriesRepository.getStory((long) 1).toFlowable(BackpressureStrategy.LATEST).subscribe(
                testSubscriber);

        testSubscriber.assertValueCount(1);

        StoryResponse result = testSubscriber.values().get(0);
        assertEquals(expected, result);
        testSubscriber.awaitTerminalEvent();
    }
}
