package com.okynk.hackernews.features.storydetail;


import com.okynk.hackernews.RobolectricTest;
import com.okynk.hackernews.api.model.StoryResponse;
import com.okynk.hackernews.mockdata.MockData;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.List;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Observable;
import io.reactivex.subscribers.TestSubscriber;

public class StoryDetailModelTest extends RobolectricTest {

    private StoryDetailContract.Repository mRepository;
    private StoryDetailContract.Model mModel;

    @Before
    public void setUp() throws Exception {
        mRepository = Mockito.mock(StoryDetailRepository.class);

        mModel = new StoryDetailModel(mRepository);
    }

    @Test
    public void testGetStoryDetail() throws Exception {
        final List<Long> longs = MockData.getTopStories();
        final StoryResponse response15145226 = MockData.getStoryResponse_15145226();
        final StoryResponse response15147335 = MockData.getStoryResponse_15147335();
        final StoryResponse response1 = MockData.getStoryResponse_1();
        Mockito.when(mRepository.getStory(response15145226.getId())).thenReturn(
                Observable.just(response15145226));
        Mockito.when(mRepository.getStory(response15147335.getId())).thenReturn(
                Observable.just(response15147335));
        Mockito.when(mRepository.getStory(response1.getId())).thenReturn(
                Observable.just(response1));

        TestSubscriber<List<StoryDetailVM>> testSubscriber = new TestSubscriber<>();
        mModel.getStoryDetail(longs).toFlowable(BackpressureStrategy.LATEST).subscribe(
                testSubscriber);

        testSubscriber.assertValueCount(1);
        testSubscriber.assertValue(MockData.getStoryDetail());

        testSubscriber.awaitTerminalEvent();
    }
}
