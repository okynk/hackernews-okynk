package com.okynk.hackernews.features.storydetail;


import com.okynk.hackernews.TestApp;
import com.okynk.hackernews.features.base.BasePresenterTest;
import com.okynk.hackernews.mockdata.MockData;
import com.okynk.hackernews.utils.Util;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.subscribers.TestSubscriber;

import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class StoryDetailPresenterTest extends BasePresenterTest {

    private StoryDetailContract.Model mModel;
    private StoryDetailContract.View mView;
    private StoryDetailPresenter mPresenter;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        mModel = mock(StoryDetailContract.Model.class);
        mView = mock(StoryDetailContract.View.class);

        mPresenter = new StoryDetailPresenter(mModel, mock(TestApp.class), subscribeOn, observeOn);
        mPresenter.start(mView);
    }

    @Test
    public void testGetData() throws Exception {
        final List<Long> storyIds = MockData.getTopStories();
        Mockito.when(mModel.getStoryDetail(storyIds)).thenReturn(
                Observable.just(MockData.getStoryDetail()));

        mPresenter.setStoryIds(storyIds);
        mPresenter.getData();

        verify(mModel, times(1)).getStoryDetail(storyIds);
        verify(mView, times(1)).setData(MockData.getStoryDetail());
        verify(mView, times(1)).setSwipeRefreshing(true);
        verify(mView, times(1)).setSwipeRefreshing(false);
        verify(mView, never()).showSnackBar(anyString());
    }

    @Test
    public void testGetData_Error() throws Exception {
        final String errorMessage = "errorMessage";
        final Throwable exception = new Throwable(errorMessage);
        final List<Long> storyIds = MockData.getTopStories();
        Mockito.when(mModel.getStoryDetail(storyIds)).thenReturn(
                Observable.<List<StoryDetailVM>> error(exception));

        mPresenter.setStoryIds(storyIds);
        mPresenter.getData();

        verify(mModel, times(1)).getStoryDetail(storyIds);
        verify(mView, never()).setData(ArgumentMatchers.<StoryDetailVM> anyList());
        verify(mView, times(1)).setSwipeRefreshing(true);
        verify(mView, times(1)).setSwipeRefreshing(false);
        verify(mView, times(1)).showSnackBar(errorMessage);
    }

    @Test
    public void testGetData_networkNotAvailable() throws Exception {
        final Field field = mPresenter.getClass().getDeclaredField("mNetworkAvailable");
        field.setAccessible(true);
        field.set(mPresenter, false);

        mPresenter.getData();

        verify(mModel, never()).getStoryDetail(ArgumentMatchers.<Long> anyList());
        verify(mView, never()).setData(ArgumentMatchers.<StoryDetailVM> anyList());
        verify(mView, never()).setSwipeRefreshing(anyBoolean());
        verify(mView, never()).showSnackBar(anyString());
    }

    @Test
    public void testPause() throws Exception {
        final Field field = mPresenter.getClass().getDeclaredField("mNetworkDisposable");
        field.setAccessible(true);

        TestSubscriber subscriber = new TestSubscriber();
        field.set(mPresenter, subscriber);

        mPresenter.pause();

        Disposable disposable = (Disposable) field.get(mPresenter);

        if (Util.isDisposableActive(disposable)) Assert.fail("disposable still active");
    }

    @Test
    public void testDestroy() throws Exception {
        final List<Field> disposableFields = new ArrayList<>();
        final String[] disposableKeys = new String[]{
                "mNetworkDisposable", "mGetDataDisposable"
        };

        Field disposableField;
        for (String subscription : disposableKeys) {
            disposableField = mPresenter.getClass().getDeclaredField(subscription);
            disposableField.setAccessible(true);
            disposableFields.add(disposableField);
        }

        TestSubscriber testSubscriber;
        for (Field field : disposableFields) {
            testSubscriber = new TestSubscriber();
            field.set(mPresenter, testSubscriber);
        }

        mPresenter.destroy();

        Disposable disposable;
        for (Field field : disposableFields) {
            disposable = (Disposable) field.get(mPresenter);
            if (Util.isDisposableActive(disposable)) {
                Assert.fail("disposable still active");
            }
        }
    }

    @Test
    public void testOnScrolled_visibleLessThanTotal() throws Exception {
        mPresenter.onScrolled(5, 5, 30);

        verify(mModel, never()).getStoryDetail(ArgumentMatchers.<Long> anyList());
        verify(mView, never()).setData(ArgumentMatchers.<StoryDetailVM> anyList());
        verify(mView, never()).setSwipeRefreshing(anyBoolean());
        verify(mView, never()).showSnackBar(anyString());
    }

    @Test
    public void testOnScrolled_notHasNextPage() throws Exception {
        final List<Long> storyIds = MockData.getTopStories(3);

        mPresenter.setStoryIds(storyIds);
        mPresenter.onScrolled(5, 5, 10);

        verify(mModel, never()).getStoryDetail(ArgumentMatchers.<Long> anyList());
        verify(mView, never()).setData(ArgumentMatchers.<StoryDetailVM> anyList());
        verify(mView, never()).setSwipeRefreshing(anyBoolean());
        verify(mView, never()).showSnackBar(anyString());
    }

    @Test
    public void testOnScrolled() throws Exception {
        final List<Long> storyIds = MockData.getTopStories(15);
        final List<Long> sublistStoryIds = storyIds.subList(10, 15);
        final List<StoryDetailVM> storyDetailVMs = MockData.getStoryDetail(sublistStoryIds);

        Mockito.when(mModel.getStoryDetail(sublistStoryIds))
                .thenReturn(Observable.just(storyDetailVMs));

        mPresenter.setStoryIds(storyIds);
        mPresenter.onScrolled(5, 5, 10);

        verify(mModel, times(1)).getStoryDetail(sublistStoryIds);
        verify(mView, times(1)).addData(storyDetailVMs);
        verify(mView, times(1)).setSwipeRefreshing(true);
        verify(mView, times(1)).setSwipeRefreshing(false);
        verify(mView, never()).showSnackBar(anyString());
    }
}
