package com.okynk.hackernews.features.base;


import android.support.annotation.NonNull;

import com.okynk.hackernews.RobolectricTest;
import com.okynk.hackernews.utils.executor.ThreadExecutor;

import org.junit.Before;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

public abstract class BasePresenterTest extends RobolectricTest {

    protected Scheduler observeOn;
    protected Scheduler subscribeOn;

    @Before
    public void setUp() throws Exception {
        subscribeOn = Schedulers.from(new ThreadExecutor() {
            @Override
            public void execute(@NonNull Runnable runnable) {
                runnable.run();
            }
        });
        observeOn = Schedulers.from(new ThreadExecutor() {
            @Override
            public void execute(@NonNull Runnable runnable) {
                runnable.run();
            }
        });
    }
}
