package com.okynk.hackernews.features.storydetail;


import com.okynk.hackernews.api.model.StoryResponse;
import com.okynk.hackernews.features.base.BaseRepositoryTest;
import com.okynk.hackernews.mockdata.MockData;

import org.junit.Before;
import org.junit.Test;

import io.reactivex.BackpressureStrategy;
import io.reactivex.subscribers.TestSubscriber;

import static org.junit.Assert.assertEquals;

public class StoryDetailRepositoryTest extends BaseRepositoryTest {

    private StoryDetailContract.Repository mStoryDetailRepository;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        mStoryDetailRepository = new StoryDetailRepository(mockApiService);
    }

    @Test
    public void testGetStory() throws Exception {
        StoryResponse expected = MockData.getStoryResponse_1();

        TestSubscriber<StoryResponse> testSubscriber = new TestSubscriber<>();
        mStoryDetailRepository.getStory((long) 1).toFlowable(BackpressureStrategy.LATEST).subscribe(
                testSubscriber);

        testSubscriber.assertValueCount(1);

        StoryResponse result = testSubscriber.values().get(0);
        assertEquals(expected, result);
        testSubscriber.awaitTerminalEvent();
    }
}
