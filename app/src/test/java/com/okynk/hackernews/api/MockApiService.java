package com.okynk.hackernews.api;


import com.okynk.hackernews.api.model.StoryResponse;
import com.okynk.hackernews.mockdata.MockData;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.Path;
import retrofit2.mock.BehaviorDelegate;

public class MockApiService implements ApiService {

    private final BehaviorDelegate<ApiService> delegate;

    public MockApiService(BehaviorDelegate<ApiService> delegate) {
        this.delegate = delegate;
    }

    @Override
    public Observable<List<Long>> getTopStories() {
        return delegate.returningResponse(MockData.getTopStories()).getTopStories();
    }

    @Override
    public Observable<StoryResponse> getStory(@Path("storyId") long storyId) {
        return delegate.returningResponse(MockData.getStoryResponse_1()).getStory(storyId);
    }
}
