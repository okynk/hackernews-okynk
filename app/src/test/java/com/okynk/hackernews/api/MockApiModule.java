package com.okynk.hackernews.api;

import dagger.Module;
import dagger.Provides;

@Module
public class MockApiModule extends ApiModule {

    @Provides
    public ApiService provideApiService() {
        return provideRetrofit(ConstantUrl.BASE_API_URL, provideClient()).create(ApiService.class);
    }
}
