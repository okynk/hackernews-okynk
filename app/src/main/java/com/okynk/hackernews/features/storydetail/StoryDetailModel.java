package com.okynk.hackernews.features.storydetail;


import com.okynk.hackernews.api.model.StoryResponse;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;

public class StoryDetailModel implements StoryDetailContract.Model {

    private final StoryDetailContract.Repository mRepository;

    public StoryDetailModel(StoryDetailContract.Repository repository) {
        mRepository = repository;
    }

    @Override
    public Observable<List<StoryDetailVM>> getStoryDetail(List<Long> storyIds) {
        return Observable.fromIterable(storyIds).flatMap(
                new Function<Long, Observable<StoryDetailVM>>() {
                    @Override
                    public Observable<StoryDetailVM> apply(@NonNull Long storyId) throws Exception {
                        return getStory(storyId, 1).mergeWith(getStory(storyId, 1).flatMap(
                                new Function<StoryDetailVM, Observable<StoryDetailVM>>() {
                                    @Override
                                    public Observable<StoryDetailVM> apply(
                                            @NonNull StoryDetailVM storyDetailVM) throws Exception {
                                        return Observable.fromIterable(
                                                storyDetailVM.getStoryVM().getKids()).flatMap(
                                                new Function<Long, Observable<StoryDetailVM>>() {
                                                    @Override
                                                    public Observable<StoryDetailVM> apply(
                                                            @NonNull Long idLevel2)
                                                            throws Exception {
                                                        return getStory(idLevel2, 2);
                                                    }
                                                });
                                    }
                                }));
                    }
                }).toList().toObservable();
    }

    private Observable<StoryDetailVM> getStory(long id, final int level) {
        return mRepository.getStory(id).flatMap(
                new Function<StoryResponse, Observable<StoryDetailVM>>() {
                    @Override
                    public Observable<StoryDetailVM> apply(@NonNull StoryResponse storyResponse)
                            throws Exception {
                        return Observable.just(new StoryDetailVM(storyResponse, level));
                    }
                });
    }

}
