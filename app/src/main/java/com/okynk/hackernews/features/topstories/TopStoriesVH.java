package com.okynk.hackernews.features.topstories;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.okynk.hackernews.R;
import com.okynk.hackernews.utils.FormattingUtil;

import java.util.concurrent.TimeUnit;

public class TopStoriesVH extends RecyclerView.ViewHolder {

    private final Context mContext;
    @BindView(R.id.txt_title)
    TextView txtTitle;
    @BindView(R.id.txt_subtitle)
    TextView txtSubtitle;

    public TopStoriesVH(Context context, View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        mContext = context;
    }

    void bindView(StoryVM storyVM) {
        txtTitle.setText(
                FormattingUtil.formatStoryTitle(mContext, storyVM.getTitle(), storyVM.getUrl()));
        txtSubtitle.setText(
                mContext.getString(R.string.topstories_format_subtitle, storyVM.getScore(),
                        storyVM.getBy(),
                        FormattingUtil.formatToRelativeDate(storyVM.getTime(), TimeUnit.SECONDS,
                                "dd MMM yyyy"), storyVM.getDescendant()));
    }
}
