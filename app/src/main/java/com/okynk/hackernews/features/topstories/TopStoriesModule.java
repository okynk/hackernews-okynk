package com.okynk.hackernews.features.topstories;

import android.content.Context;

import com.okynk.hackernews.api.ApiService;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;

@Module
public class TopStoriesModule {

    @Provides
    public TopStoriesContract.Presenter providePresenter(TopStoriesContract.Model model,
                                                         Context context, @Named("subscribeOn")
                                                                 Scheduler subscribeOn,
                                                         @Named("observeOn") Scheduler observeOn) {
        return new TopStoriesPresenter(model, context, subscribeOn, observeOn);
    }

    @Provides
    public TopStoriesContract.Model provideModel(TopStoriesContract.Repository repository) {
        return new TopStoriesModel(repository);
    }

    @Provides
    public TopStoriesContract.Repository provideRepository(ApiService apiService) {
        return new TopStoriesRepository(apiService);
    }
}
