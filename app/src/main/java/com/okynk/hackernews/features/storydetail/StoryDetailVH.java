package com.okynk.hackernews.features.storydetail;


import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.okynk.hackernews.R;
import com.okynk.hackernews.utils.FormattingUtil;

import java.util.concurrent.TimeUnit;

public class StoryDetailVH extends RecyclerView.ViewHolder {

    private final Context mContext;

    @BindView(R.id.container)
    LinearLayout container;
    @BindView(R.id.txt_poster)
    TextView txtPoster;
    @BindView(R.id.txt_content)
    TextView txtContent;
    @BindView(R.id.divider_bottom)
    View dividerBottom;

    public StoryDetailVH(Context context, View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        mContext = context;
        txtContent.setMovementMethod(LinkMovementMethod.getInstance());
    }

    void bindView(StoryDetailVM storyDetailVM) {
        txtPoster.setText(mContext.getString(R.string.storydetail_format_poster,
                storyDetailVM.getStoryVM().getBy(),
                FormattingUtil.formatToRelativeDate(storyDetailVM.getStoryVM().getTime(),
                        TimeUnit.SECONDS, "dd MMM yyyy")));
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            txtContent.setText(FormattingUtil.trimWhiteLines(
                    Html.fromHtml(storyDetailVM.getStoryVM().getText(),
                            Html.FROM_HTML_MODE_COMPACT)));
        } else {
            txtContent.setText(FormattingUtil.trimWhiteLines(
                    Html.fromHtml(storyDetailVM.getStoryVM().getText())));
        }

        final int spacingNormal = mContext.getResources().getDimensionPixelSize(
                R.dimen.spacing_normal);
        final int spacingSmall = mContext.getResources().getDimensionPixelSize(
                R.dimen.spacing_small);
        container.setPadding(storyDetailVM.getLevel() * spacingNormal, spacingSmall, spacingNormal,
                0);
    }
}
