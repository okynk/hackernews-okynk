package com.okynk.hackernews.features.topstories;


import com.okynk.hackernews.api.model.StoryResponse;

import java.util.List;

import io.reactivex.Observable;

public interface TopStoriesContract {

    interface View {

        void setData(List<StoryVM> data);

        void addData(List<StoryVM> data);

        void showSnackBar(String message);

        void setSwipeRefreshing(boolean refreshing);

        void onNetworkAvailabilityChange(boolean available);
    }

    interface Presenter {

        void start(View view);

        void getData();

        void resume();

        void pause();

        void getNextPage();

        void onScrolled(int visibleItemCount, int pastVisibleItemCount, int totalItemCount);

        void destroy();
    }

    interface Model {

        Observable<List<Long>> getTopStories();

        Observable<List<StoryVM>> getStories(List<Long> storyIds);
    }

    interface Repository {

        Observable<List<Long>> getTopStories();

        Observable<StoryResponse> getStory(long storyId);
    }

}
