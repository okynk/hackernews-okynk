package com.okynk.hackernews.features.topstories;


import android.os.Parcel;
import android.os.Parcelable;

import com.okynk.hackernews.api.model.StoryResponse;

import java.util.ArrayList;
import java.util.List;

public class StoryVM implements Parcelable {

    public static final Creator<StoryVM> CREATOR = new Creator<StoryVM>() {
        @Override
        public StoryVM createFromParcel(Parcel in) {
            return new StoryVM(in);
        }

        @Override
        public StoryVM[] newArray(int size) {
            return new StoryVM[size];
        }
    };
    private long mId;
    private String mBy;
    private int mScore;
    private long mTime;
    private String mTitle;
    private List<Long> mKids;
    private String mUrl;
    private StoryType mType;
    private long mDescendant;
    private String mText;

    public StoryVM() {
    }

    public StoryVM(StoryResponse storyResponse) {
        mId = storyResponse.getId();
        mBy = storyResponse.getBy();
        mScore = storyResponse.getScore();
        mTime = storyResponse.getTime();
        mTitle = storyResponse.getTitle();
        mKids = new ArrayList<>();
        if (storyResponse.getKids() != null) {
            mKids.addAll(storyResponse.getKids());
        }
        mUrl = storyResponse.getUrl();
        mType = StoryType.getInstance(storyResponse.getType());
        mDescendant = storyResponse.getDescendants();
        mText = storyResponse.getText();
    }

    protected StoryVM(Parcel in) {
        mId = in.readLong();
        mBy = in.readString();
        mScore = in.readInt();
        mTime = in.readLong();
        mTitle = in.readString();
        mUrl = in.readString();
        mDescendant = in.readLong();
        mText = in.readString();
        mKids = new ArrayList<>();
        in.readList(mKids, List.class.getClassLoader());
        mType = (StoryType) in.readSerializable();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(mId);
        dest.writeString(mBy);
        dest.writeInt(mScore);
        dest.writeLong(mTime);
        dest.writeString(mTitle);
        dest.writeString(mUrl);
        dest.writeLong(mDescendant);
        dest.writeString(mText);
        dest.writeList(mKids);
        dest.writeSerializable(mType);
    }

    @Override
    public int hashCode() {
        int result = (int) (mId ^ (mId >>> 32));
        result = 31 * result + (mBy != null ? mBy.hashCode() : 0);
        result = 31 * result + mScore;
        result = 31 * result + (int) (mTime ^ (mTime >>> 32));
        result = 31 * result + (mTitle != null ? mTitle.hashCode() : 0);
        result = 31 * result + (mKids != null ? mKids.hashCode() : 0);
        result = 31 * result + (mUrl != null ? mUrl.hashCode() : 0);
        result = 31 * result + (mType != null ? mType.hashCode() : 0);
        result = 31 * result + (int) (mDescendant ^ (mDescendant >>> 32));
        result = 31 * result + (mText != null ? mText.hashCode() : 0);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StoryVM storyVM = (StoryVM) o;

        if (mId != storyVM.mId) return false;
        if (mScore != storyVM.mScore) return false;
        if (mTime != storyVM.mTime) return false;
        if (mDescendant != storyVM.mDescendant) return false;
        if (mBy != null ? !mBy.equals(storyVM.mBy) : storyVM.mBy != null) return false;
        if (mTitle != null ? !mTitle.equals(storyVM.mTitle) : storyVM.mTitle != null) return false;
        if (mKids != null ? !mKids.equals(storyVM.mKids) : storyVM.mKids != null) return false;
        if (mUrl != null ? !mUrl.equals(storyVM.mUrl) : storyVM.mUrl != null) return false;
        if (mType != storyVM.mType) return false;
        return mText != null ? mText.equals(storyVM.mText) : storyVM.mText == null;

    }

    public long getId() {
        return mId;
    }

    public String getBy() {
        return mBy;
    }

    public int getScore() {
        return mScore;
    }

    public long getTime() {
        return mTime;
    }

    public String getTitle() {
        return mTitle;
    }

    public List<Long> getKids() {
        return mKids;
    }

    public String getUrl() {
        return mUrl;
    }

    public StoryType getType() {
        return mType;
    }

    public long getDescendant() {
        return mDescendant;
    }

    public String getText() {
        return mText;
    }
}
