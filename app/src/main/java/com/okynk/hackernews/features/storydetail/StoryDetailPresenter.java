package com.okynk.hackernews.features.storydetail;


import android.content.Context;
import android.net.NetworkInfo;

import com.github.pwittchen.reactivenetwork.library.rx2.Connectivity;
import com.github.pwittchen.reactivenetwork.library.rx2.ReactiveNetwork;
import com.okynk.hackernews.utils.Util;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import timber.log.Timber;

public class StoryDetailPresenter implements StoryDetailContract.Presenter {

    private static final int NUM_PER_PAGE = 10;

    private final StoryDetailContract.Model mModel;
    private final Context mApplicationContext;
    private StoryDetailContract.View mView;
    private boolean mNetworkAvailable;
    private Disposable mNetworkDisposable;
    private Disposable mGetDataDisposable;
    private Scheduler mSubscribeOn;
    private Scheduler mObserveOn;
    private List<Long> mStoryIds;
    private int mCurrentPage = 1;

    public StoryDetailPresenter(StoryDetailContract.Model model, Context applicationContext,
                                Scheduler subscribeOn, Scheduler observeOn) {
        mModel = model;
        mApplicationContext = applicationContext;
        mNetworkAvailable = true;
        mSubscribeOn = subscribeOn;
        mObserveOn = observeOn;
        mStoryIds = new ArrayList<>();
    }

    @Override
    public void start(StoryDetailContract.View view) {
        mView = view;
    }

    private boolean hasNextPage() {
        return mStoryIds.size() >= (((mCurrentPage - 1) * NUM_PER_PAGE) + NUM_PER_PAGE);
    }

    private List<Long> getStoryIds() {
        int startIndex = (mCurrentPage - 1) * NUM_PER_PAGE;
        int endIndex = startIndex + NUM_PER_PAGE;
        endIndex = Math.min(endIndex, mStoryIds.size());
        return mStoryIds.subList(startIndex, endIndex);
    }

    @Override
    public void setStoryIds(List<Long> storyIds) {
        mStoryIds.clear();
        mStoryIds.addAll(storyIds);
    }

    @Override
    public void onScrolled(int visibleItemCount, int pastVisibleItemCount, int totalItemCount) {
        final int totalVisibleItems = visibleItemCount + pastVisibleItemCount;
        if (totalVisibleItems >= (totalItemCount - (NUM_PER_PAGE)) && hasNextPage()) {
            mCurrentPage++;
            getData();
        }
    }

    @Override
    public void getData() {
        if (mNetworkAvailable) {
            mView.setSwipeRefreshing(true);
            mModel.getStoryDetail(getStoryIds())
                    .subscribeOn(mSubscribeOn)
                    .observeOn(mObserveOn)
                    .doAfterTerminate(new Action() {
                        @Override
                        public void run() throws Exception {
                            mGetDataDisposable = null;
                        }
                    }).subscribe(new Observer<List<StoryDetailVM>>() {
                @Override
                public void onSubscribe(@NonNull Disposable d) {
                    mGetDataDisposable = d;
                }

                @Override
                public void onNext(@NonNull List<StoryDetailVM> storyDetailVMs) {
                    if (mCurrentPage == 1) {
                        mView.setData(storyDetailVMs);
                    } else {
                        mView.addData(storyDetailVMs);
                    }
                }

                @Override
                public void onError(@NonNull Throwable e) {
                    Timber.d("error refresh : " + e.getMessage());
                    mView.showSnackBar(e.getMessage());
                    mView.setSwipeRefreshing(false);
                }

                @Override
                public void onComplete() {
                    mView.setSwipeRefreshing(false);
                }
            });
        }
    }

    @Override
    public void resume() {
        mNetworkDisposable = ReactiveNetwork.observeNetworkConnectivity(mApplicationContext)
                .subscribeOn(mSubscribeOn)
                .observeOn(mObserveOn)
                .subscribe(new Consumer<Connectivity>() {
                    @Override
                    public void accept(Connectivity connectivity) throws Exception {
                        Timber.d("connectivity : " + (connectivity.getState()
                                == NetworkInfo.State.CONNECTED));
                        mNetworkAvailable = (connectivity.getState()
                                == NetworkInfo.State.CONNECTED);
                        mView.onNetworkAvailabilityChange(mNetworkAvailable);
                        if (!mNetworkAvailable) {
                            mView.setSwipeRefreshing(false);
                            Util.safelyDispose(mGetDataDisposable);
                        }
                    }
                });
    }

    @Override
    public void pause() {
        Util.safelyDispose(mNetworkDisposable);
    }

    @Override
    public void destroy() {
        Util.safelyDispose(mGetDataDisposable, mNetworkDisposable);
    }
}
