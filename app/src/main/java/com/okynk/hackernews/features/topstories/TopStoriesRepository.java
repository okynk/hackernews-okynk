package com.okynk.hackernews.features.topstories;


import com.okynk.hackernews.api.ApiService;
import com.okynk.hackernews.api.model.StoryResponse;

import java.util.List;

import io.reactivex.Observable;

public class TopStoriesRepository implements TopStoriesContract.Repository {

    private final ApiService mApiService;

    public TopStoriesRepository(ApiService apiService) {
        mApiService = apiService;
    }

    @Override
    public Observable<List<Long>> getTopStories() {
        return mApiService.getTopStories();
    }

    @Override
    public Observable<StoryResponse> getStory(long storyId) {
        return mApiService.getStory(storyId);
    }
}
