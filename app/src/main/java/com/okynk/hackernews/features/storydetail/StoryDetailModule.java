package com.okynk.hackernews.features.storydetail;

import android.content.Context;

import com.okynk.hackernews.api.ApiService;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;

@Module
public class StoryDetailModule {

    @Provides
    public StoryDetailContract.Presenter providesPresenter(StoryDetailContract.Model model,
                                                           Context context, @Named("subscribeOn")
                                                                   Scheduler subscribeOn,
                                                           @Named("observeOn")
                                                                   Scheduler observeOn) {
        return new StoryDetailPresenter(model, context, subscribeOn, observeOn);
    }

    @Provides
    public StoryDetailContract.Model providesModel(StoryDetailContract.Repository repository) {
        return new StoryDetailModel(repository);
    }

    @Provides
    public StoryDetailContract.Repository providesRepository(ApiService apiService) {
        return new StoryDetailRepository(apiService);
    }
}
