package com.okynk.hackernews.features.storydetail;


import com.okynk.hackernews.api.ApiService;
import com.okynk.hackernews.api.model.StoryResponse;

import io.reactivex.Observable;

public class StoryDetailRepository implements StoryDetailContract.Repository {

    private final ApiService mApiService;

    public StoryDetailRepository(ApiService apiService) {
        mApiService = apiService;
    }

    @Override
    public Observable<StoryResponse> getStory(long storyId) {
        return mApiService.getStory(storyId);
    }
}
