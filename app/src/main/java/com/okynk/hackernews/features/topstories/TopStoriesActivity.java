package com.okynk.hackernews.features.topstories;


import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.okynk.hackernews.R;
import com.okynk.hackernews.features.base.BaseActivity;
import com.okynk.hackernews.features.storydetail.StoryDetailActivity;
import com.okynk.hackernews.root.App;
import com.okynk.hackernews.utils.Util;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class TopStoriesActivity extends BaseActivity
        implements TopStoriesContract.View, TopStoriesAdapter.Listener,
                   SwipeRefreshLayout.OnRefreshListener {

    private static final String BUNDLE_DATA = "BUNDLE_DATA";
    private static final String BUNDLE_LIST_STATE = "BUNDLE_LIST_STATE";

    @Inject
    TopStoriesContract.Presenter presenter;
    private final RecyclerView.OnScrollListener mOnScrollListener
            = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            LinearLayoutManager layoutManager
                    = (LinearLayoutManager) recyclerView.getLayoutManager();
            if (layoutManager == null) {
                return;
            }

            presenter.onScrolled(layoutManager.getChildCount(),
                    layoutManager.findFirstVisibleItemPosition(), layoutManager.getItemCount());
        }
    };
    @BindView(R.id.container_master)
    CoordinatorLayout containerMaster;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.rv_top_stories)
    RecyclerView rvTopStories;
    private TopStoriesAdapter mAdapter;
    private List<StoryVM> mData;
    private Parcelable mListState;
    private Snackbar mSnackbarNetwork;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_stories);
        ((App) getApplication()).getApplicationComponent().inject(this);
        ButterKnife.bind(this);
        presenter.start(this);

        setTitle(R.string.topstories_title);

        initComponent();
    }

    @Override
    protected void onDestroy() {
        presenter.destroy();
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mListState = rvTopStories.getLayoutManager().onSaveInstanceState();
        outState.putParcelable(BUNDLE_LIST_STATE, mListState);
        outState.putParcelableArrayList(BUNDLE_DATA, new ArrayList<>(mAdapter.getData()));
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.resume();

        if (mData != null) {
            mAdapter.setData(mData);
        }
        if (mListState != null) {
            rvTopStories.getLayoutManager().onRestoreInstanceState(mListState);
        }
        if (!mAdapter.hasData()) {
            presenter.getData();
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            mListState = savedInstanceState.getParcelable(BUNDLE_LIST_STATE);
            mData = savedInstanceState.getParcelableArrayList(BUNDLE_DATA);
        }
    }

    @Override
    public void setData(List<StoryVM> data) {
        mAdapter.setData(data);
    }

    @Override
    public void addData(List<StoryVM> data) {
        mAdapter.addData(data);
    }

    @Override
    public void showSnackBar(String message) {
        if (!Util.isStringEmpty(message)) {
            Snackbar.make(containerMaster, message, Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    public void setSwipeRefreshing(boolean refreshing) {
        Util.setSwipeRefreshing(swipeRefresh, refreshing);
    }

    @Override
    public void onNetworkAvailabilityChange(boolean available) {
        swipeRefresh.setEnabled(available);
        if (available) {
            mSnackbarNetwork.dismiss();
        } else {
            mSnackbarNetwork.show();
        }
    }

    @Override
    public void onClickStory(StoryVM storyVM) {
        startActivity(StoryDetailActivity.newIntent(this, storyVM));
    }

    @Override
    public void onRefresh() {
        presenter.getData();
    }

    private void initComponent() {
        mAdapter = new TopStoriesAdapter(this);
        swipeRefresh.setOnRefreshListener(this);
        rvTopStories.setAdapter(mAdapter);
        rvTopStories.setLayoutManager(
                new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvTopStories.setItemAnimator(new DefaultItemAnimator());
        rvTopStories.addOnScrollListener(mOnScrollListener);
        rvTopStories.addItemDecoration(new HorizontalDividerItemDecoration.Builder(this).colorResId(
                R.color.divider)
                .sizeResId(R.dimen.line_height)
                .marginResId(R.dimen.spacing_normal)
                .build());
        mSnackbarNetwork = Snackbar.make(containerMaster, getString(R.string.general_error_network),
                Snackbar.LENGTH_INDEFINITE);
    }
}
