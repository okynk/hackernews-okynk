package com.okynk.hackernews.features.storydetail;


import android.support.annotation.StringRes;

import com.okynk.hackernews.api.model.StoryResponse;

import java.util.List;

import io.reactivex.Observable;

public class StoryDetailContract {

    interface View {

        void setData(List<StoryDetailVM> data);

        void addData(List<StoryDetailVM> data);

        void showSnackBar(String message);

        void showSnackBar(@StringRes int messageResId);

        void setSwipeRefreshing(boolean refreshing);

        void onNetworkAvailabilityChange(boolean available);
    }

    interface Presenter {

        void start(View view);

        void setStoryIds(List<Long> storyIds);

        void onScrolled(int visibleItemCount, int pastVisibleItemCount, int totalItemCount);

        void getData();

        void resume();

        void pause();

        void destroy();
    }

    interface Model {

        Observable<List<StoryDetailVM>> getStoryDetail(List<Long> storyIds);
    }

    interface Repository {

        Observable<StoryResponse> getStory(long storyId);
    }

}
