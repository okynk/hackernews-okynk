package com.okynk.hackernews.features.topstories;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.okynk.hackernews.R;
import com.okynk.hackernews.utils.Util;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

public class TopStoriesAdapter extends RecyclerView.Adapter<TopStoriesVH> {

    private List<StoryVM> mData;
    private Listener mListener;

    public TopStoriesAdapter(Listener listener) {
        mData = new ArrayList<>();
        mListener = listener;
    }

    @Override
    public TopStoriesVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_top_stories,
                parent, false);
        final TopStoriesVH vh = new TopStoriesVH(parent.getContext(), view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final int pos = vh.getAdapterPosition();
                if (pos != RecyclerView.NO_POSITION && mListener != null) {
                    mListener.onClickStory(mData.get(pos));
                }
            }
        });

        return vh;
    }

    @Override
    public void onBindViewHolder(TopStoriesVH holder, int position) {
        Timber.d("topstories position : " + position);
        holder.bindView(mData.get(position));
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void addData(List<StoryVM> data) {
        mData.addAll(data);
        notifyDataSetChanged();
    }

    public boolean hasData() {
        return !Util.isCollectionEmpty(mData);
    }

    public List<StoryVM> getData() {
        return mData;
    }

    public void setData(List<StoryVM> data) {
        mData.clear();
        addData(data);
    }

    interface Listener {

        void onClickStory(StoryVM storyVM);
    }
}
