package com.okynk.hackernews.features.topstories;


import android.content.Context;
import android.net.NetworkInfo;

import com.github.pwittchen.reactivenetwork.library.rx2.Connectivity;
import com.github.pwittchen.reactivenetwork.library.rx2.ReactiveNetwork;
import com.okynk.hackernews.utils.Util;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import timber.log.Timber;

public class TopStoriesPresenter implements TopStoriesContract.Presenter {

    private static final int NUM_PER_PAGE = 20;
    private final TopStoriesContract.Model mModel;
    private final Context mApplicationContext;
    private TopStoriesContract.View mView;
    private int mCurrentPage = 1;
    private List<Long> mTopStories;
    private boolean mNetworkAvailable;
    private Disposable mNetworkDisposable;
    private Disposable mGetDataDisposable;
    private Disposable mFetchStoriesDisposable;
    private Scheduler mSubscribeOn;
    private Scheduler mObserveOn;

    public TopStoriesPresenter(TopStoriesContract.Model model, Context applicationContext,
                               Scheduler subscribeOn, Scheduler observeOn) {
        mModel = model;
        mApplicationContext = applicationContext;
        mTopStories = new ArrayList<>();
        mNetworkAvailable = true;
        mSubscribeOn = subscribeOn;
        mObserveOn = observeOn;
    }

    @Override
    public void start(TopStoriesContract.View view) {
        mView = view;
    }

    @Override
    public void getData() {
        if (mNetworkAvailable) {
            mView.setSwipeRefreshing(true);
            mModel.getTopStories().subscribeOn(mSubscribeOn).observeOn(mObserveOn).doAfterTerminate(
                    new Action() {
                        @Override
                        public void run() throws Exception {
                            mGetDataDisposable = null;
                        }
                    }).subscribe(new Observer<List<Long>>() {
                @Override
                public void onSubscribe(@NonNull Disposable d) {
                    mGetDataDisposable = d;
                }

                @Override
                public void onNext(@NonNull List<Long> topStories) {
                    mTopStories.clear();
                    mTopStories.addAll(topStories);
                }

                @Override
                public void onError(@NonNull Throwable e) {
                    Timber.d("error refresh : " + e.getMessage());
                    mView.showSnackBar(e.getMessage());
                    mView.setSwipeRefreshing(false);
                }

                @Override
                public void onComplete() {
                    mCurrentPage = 1;
                    fetchStories();
                }
            });
        }
    }

    @Override
    public void resume() {
        mNetworkDisposable = ReactiveNetwork.observeNetworkConnectivity(mApplicationContext)
                .subscribeOn(mSubscribeOn)
                .observeOn(mObserveOn)
                .subscribe(new Consumer<Connectivity>() {
                    @Override
                    public void accept(Connectivity connectivity) throws Exception {
                        Timber.d("connectivity : " + (connectivity.getState()
                                == NetworkInfo.State.CONNECTED));
                        mNetworkAvailable = (connectivity.getState()
                                == NetworkInfo.State.CONNECTED);
                        mView.onNetworkAvailabilityChange(mNetworkAvailable);
                        if (!mNetworkAvailable) {
                            mView.setSwipeRefreshing(false);
                            Util.safelyDispose(mFetchStoriesDisposable, mGetDataDisposable);
                        }
                    }
                });
    }

    @Override
    public void pause() {
        Util.safelyDispose(mNetworkDisposable);
    }

    @Override
    public void getNextPage() {
        if (mNetworkAvailable && hasNextPage()) {
            mView.setSwipeRefreshing(true);
            mCurrentPage++;
            fetchStories();
        }
    }

    @Override
    public void onScrolled(int visibleItemCount, int pastVisibleItemCount, int totalItemCount) {
        final int totalVisibleItems = visibleItemCount + pastVisibleItemCount;
        if (totalVisibleItems >= (totalItemCount - (NUM_PER_PAGE))) {
            getNextPage();
        }
    }

    @Override
    public void destroy() {
        Util.safelyDispose(mFetchStoriesDisposable, mGetDataDisposable, mNetworkDisposable);
    }

    private void fetchStories() {
        mModel.getStories(getStoryIds()).subscribeOn(mSubscribeOn).doAfterTerminate(new Action() {
            @Override
            public void run() throws Exception {
                mFetchStoriesDisposable = null;
            }
        }).observeOn(mObserveOn).subscribe(new Observer<List<StoryVM>>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {
                mFetchStoriesDisposable = d;
            }

            @Override
            public void onNext(@NonNull List<StoryVM> storyVMs) {
                if (mCurrentPage == 1) {
                    mView.setData(storyVMs);
                } else {
                    mView.addData(storyVMs);
                }
            }

            @Override
            public void onError(@NonNull Throwable e) {
                Timber.d("error fetch : " + e.getMessage());
                mView.showSnackBar(e.getMessage());
                mView.setSwipeRefreshing(false);
            }

            @Override
            public void onComplete() {
                mView.setSwipeRefreshing(false);
            }
        });
    }

    private List<Long> getStoryIds() {
        int startIndex = (mCurrentPage - 1) * NUM_PER_PAGE;
        int endIndex = startIndex + NUM_PER_PAGE;
        endIndex = Math.min(endIndex, mTopStories.size());
        return mTopStories.subList(startIndex, endIndex);
    }

    private boolean hasNextPage() {
        return mTopStories.size() >= (((mCurrentPage - 1) * NUM_PER_PAGE) + NUM_PER_PAGE);
    }
}
