package com.okynk.hackernews.features.topstories;


import com.okynk.hackernews.api.model.StoryResponse;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;

public class TopStoriesModel implements TopStoriesContract.Model {

    private final TopStoriesContract.Repository mRepository;

    public TopStoriesModel(TopStoriesContract.Repository repository) {
        mRepository = repository;
    }

    @Override
    public Observable<List<Long>> getTopStories() {
        return mRepository.getTopStories();
    }

    @Override
    public Observable<List<StoryVM>> getStories(List<Long> storyIds) {
        return Observable.fromIterable(storyIds).flatMap(new Function<Long, Observable<StoryVM>>() {
            @Override
            public Observable<StoryVM> apply(@NonNull Long storyId) throws Exception {
                return mRepository.getStory(storyId).flatMap(
                        new Function<StoryResponse, Observable<StoryVM>>() {
                            @Override
                            public Observable<StoryVM> apply(@NonNull StoryResponse storyResponse)
                                    throws Exception {
                                return Observable.just(new StoryVM(storyResponse));
                            }
                        });
            }
        }).toList().toObservable();
    }
}
