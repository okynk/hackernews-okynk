package com.okynk.hackernews.features.storydetail;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.okynk.hackernews.R;
import com.okynk.hackernews.utils.Util;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

public class StoryDetailAdapter extends RecyclerView.Adapter<StoryDetailVH> {

    private List<StoryDetailVM> mData;

    public StoryDetailAdapter() {
        mData = new ArrayList<>();
    }

    @Override
    public StoryDetailVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_story_detail,
                parent, false);
        return new StoryDetailVH(parent.getContext(), view);
    }

    @Override
    public void onBindViewHolder(StoryDetailVH holder, int position) {
        Timber.d("storydetail position : " + position);
        final StoryDetailVM currItem = mData.get(position);
        holder.bindView(currItem);
        if (position + 1 != mData.size()) {
            final StoryDetailVM nextItem = mData.get(position + 1);
            if (nextItem.getLevel() == 1) {
                holder.dividerBottom.setVisibility(View.VISIBLE);
            } else {
                holder.dividerBottom.setVisibility(View.INVISIBLE);
            }
        } else {
            holder.dividerBottom.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public List<StoryDetailVM> getData() {
        return mData;
    }

    public void setData(List<StoryDetailVM> data) {
        mData.clear();
        addData(data);
    }

    public void addData(List<StoryDetailVM> data) {
        mData.addAll(data);
        notifyDataSetChanged();
    }

    public boolean hasData() {
        return !Util.isCollectionEmpty(mData);
    }
}
