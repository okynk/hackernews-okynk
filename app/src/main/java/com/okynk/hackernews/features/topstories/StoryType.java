package com.okynk.hackernews.features.topstories;


import android.support.annotation.NonNull;

import java.util.HashMap;
import java.util.Map;

import timber.log.Timber;

public enum StoryType {
    STORY("story"),
    COMMENT("comment"),
    UNKNOWN("");

    private static final Map<String, StoryType> MAP = initIdToInstance();
    private String mType;

    StoryType(String type) {
        mType = type;
    }

    @NonNull
    public static StoryType getInstance(String type) {
        StoryType storyType = MAP.get(type);
        if (storyType == null) {
            Timber.i("Story type %s is not found", type);
            return UNKNOWN;
        } else {
            return storyType;
        }
    }

    private static Map<String, StoryType> initIdToInstance() {
        final StoryType[] storyTypes = values();
        final Map<String, StoryType> map = new HashMap<>(storyTypes.length);
        for (StoryType storyType : storyTypes) {
            map.put(storyType.getType(), storyType);
        }
        return map;
    }

    public String getType() {
        return mType;
    }

}
