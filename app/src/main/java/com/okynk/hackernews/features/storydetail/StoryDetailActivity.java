package com.okynk.hackernews.features.storydetail;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.okynk.hackernews.R;
import com.okynk.hackernews.features.base.BaseActivity;
import com.okynk.hackernews.features.topstories.StoryVM;
import com.okynk.hackernews.root.App;
import com.okynk.hackernews.utils.Util;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class StoryDetailActivity extends BaseActivity
        implements StoryDetailContract.View, SwipeRefreshLayout.OnRefreshListener {

    private static final String BUNDLE_DATA = "BUNDLE_DATA";
    private static final String BUNDLE_LIST_STATE = "BUNDLE_LIST_STATE";
    private static final String EXTRA_STORY_VM = "EXTRA_STORY_VM";

    @Inject
    StoryDetailContract.Presenter presenter;
    private final RecyclerView.OnScrollListener mOnScrollListener
            = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            LinearLayoutManager layoutManager
                    = (LinearLayoutManager) recyclerView.getLayoutManager();
            if (layoutManager == null) {
                return;
            }

            presenter.onScrolled(layoutManager.getChildCount(),
                    layoutManager.findFirstVisibleItemPosition(), layoutManager.getItemCount());
        }
    };
    @BindView(R.id.container_master)
    CoordinatorLayout containerMaster;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.rv_story_detail)
    RecyclerView rvStoryDetail;
    private StoryDetailAdapter mAdapter;
    private List<StoryDetailVM> mData;
    private Parcelable mListState;
    private StoryVM mStoryVM;
    private Snackbar mSnackbarNetwork;

    public static Intent newIntent(Context context, StoryVM storyVM) {
        Intent intent = new Intent(context, StoryDetailActivity.class);
        intent.putExtra(EXTRA_STORY_VM, storyVM);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_story_detail);
        ((App) getApplication()).getApplicationComponent().inject(this);
        ButterKnife.bind(this);
        presenter.start(this);

        setTitle(R.string.storydetail_title);
        enableBackButton();

        initComponent();

        mStoryVM = getIntent().getParcelableExtra(EXTRA_STORY_VM);
    }

    @Override
    protected void onDestroy() {
        presenter.destroy();
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mListState = rvStoryDetail.getLayoutManager().onSaveInstanceState();
        outState.putParcelable(BUNDLE_LIST_STATE, mListState);
        outState.putParcelableArrayList(BUNDLE_DATA, new ArrayList<>(mAdapter.getData()));
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.resume();

        if (mData != null) {
            mAdapter.setData(mData);
        }
        if (mListState != null) {
            rvStoryDetail.getLayoutManager().onRestoreInstanceState(mListState);
        }
        if (!mAdapter.hasData()) {
            presenter.setStoryIds(mStoryVM.getKids());
            presenter.getData();
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            mListState = savedInstanceState.getParcelable(BUNDLE_LIST_STATE);
            mData = savedInstanceState.getParcelableArrayList(BUNDLE_DATA);
        }
    }

    @Override
    public void setData(List<StoryDetailVM> data) {
        mAdapter.setData(data);
    }

    @Override
    public void addData(List<StoryDetailVM> data) {
        mAdapter.addData(data);
    }

    @Override
    public void showSnackBar(String message) {
        if (!Util.isStringEmpty(message)) {
            Snackbar.make(containerMaster, message, Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    public void showSnackBar(@StringRes int messageResId) {
        Snackbar.make(containerMaster, getString(messageResId), Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void setSwipeRefreshing(boolean refreshing) {
        Util.setSwipeRefreshing(swipeRefresh, refreshing);
    }

    @Override
    public void onNetworkAvailabilityChange(boolean available) {
        swipeRefresh.setEnabled(available);
        if (available) {
            mSnackbarNetwork.dismiss();
        } else {
            mSnackbarNetwork.show();
        }
    }

    @Override
    public void onRefresh() {
        presenter.getData();
    }

    private void initComponent() {
        mAdapter = new StoryDetailAdapter();
        swipeRefresh.setOnRefreshListener(this);
        rvStoryDetail.setAdapter(mAdapter);
        rvStoryDetail.addOnScrollListener(mOnScrollListener);
        rvStoryDetail.setLayoutManager(
                new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvStoryDetail.setItemAnimator(new DefaultItemAnimator());
        mSnackbarNetwork = Snackbar.make(containerMaster, getString(R.string.general_error_network),
                Snackbar.LENGTH_INDEFINITE);
    }
}
