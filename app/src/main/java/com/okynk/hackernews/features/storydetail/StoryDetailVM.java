package com.okynk.hackernews.features.storydetail;


import android.os.Parcel;
import android.os.Parcelable;

import com.okynk.hackernews.api.model.StoryResponse;
import com.okynk.hackernews.features.topstories.StoryVM;

public class StoryDetailVM implements Parcelable {

    public static final Creator<StoryDetailVM> CREATOR = new Creator<StoryDetailVM>() {
        @Override
        public StoryDetailVM createFromParcel(Parcel in) {
            return new StoryDetailVM(in);
        }

        @Override
        public StoryDetailVM[] newArray(int size) {
            return new StoryDetailVM[size];
        }
    };
    private StoryVM mStoryVM;
    private int mLevel;

    public StoryDetailVM() {
    }

    public StoryDetailVM(StoryResponse storyResponse, int level) {
        mStoryVM = new StoryVM(storyResponse);
        mLevel = level;
    }

    protected StoryDetailVM(Parcel in) {
        mStoryVM = in.readParcelable(StoryVM.class.getClassLoader());
        mLevel = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(mStoryVM, flags);
        dest.writeInt(mLevel);
    }

    @Override
    public int hashCode() {
        int result = mStoryVM != null ? mStoryVM.hashCode() : 0;
        result = 31 * result + mLevel;
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StoryDetailVM that = (StoryDetailVM) o;

        if (mLevel != that.mLevel) return false;
        return mStoryVM != null ? mStoryVM.equals(that.mStoryVM) : that.mStoryVM == null;

    }

    public StoryVM getStoryVM() {
        return mStoryVM;
    }

    public void setStoryVM(StoryVM storyVM) {
        mStoryVM = storyVM;
    }

    public int getLevel() {
        return mLevel;
    }

    public void setLevel(int level) {
        mLevel = level;
    }
}
