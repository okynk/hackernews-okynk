package com.okynk.hackernews.utils.executor;

import java.util.concurrent.Executor;

/**
 * from https://github.com/android10/Android-CleanArchitecture
 */
public interface ThreadExecutor extends Executor {

}
