package com.okynk.hackernews.utils;


import android.support.v4.widget.SwipeRefreshLayout;

import java.util.Collection;

import io.reactivex.disposables.Disposable;

public final class Util {

    public static void setSwipeRefreshing(final SwipeRefreshLayout swipeRefresh,
                                          final boolean refreshing) {
        if (swipeRefresh != null) {
            if (refreshing) {
                swipeRefresh.setRefreshing(true);
            } else {
                swipeRefresh.post(new Runnable() {
                    public void run() {
                        swipeRefresh.setRefreshing(false);
                    }
                });
            }
        }
    }

    public static boolean isCollectionEmpty(Collection collection) {
        return !(collection != null && !collection.isEmpty());
    }

    public static boolean isStringEmpty(String str) {
        return str == null || str.trim().equals("");
    }

    public static void safelyDispose(Disposable... disposables) {
        for (Disposable subscription : disposables) {
            if (isDisposableActive(subscription)) {
                subscription.dispose();
            }
        }
    }

    public static boolean isDisposableActive(Disposable disposable) {
        return disposable != null && !disposable.isDisposed();
    }
}
