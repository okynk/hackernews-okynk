package com.okynk.hackernews.utils;


import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;

import com.okynk.hackernews.R;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public final class FormattingUtil {

    private static final int TOTAL_DAYS_IN_MONTH = 31;
    private static final int TOTAL_HOUR_IN_DAY = 24;
    private static final int TOTAL_MINUTES_IN_HOUR = 60;
    private static final int TOTAL_SECONDS_IN_MINUTE = 60;
    private static Clock sClock = new Clock() {
        @Override
        public long getCurrentTimeMillis() {
            return System.currentTimeMillis();
        }
    };

    public static SpannableStringBuilder formatStoryTitle(Context context, String title,
                                                          String url) {
        SpannableStringBuilder titleSpan = new SpannableStringBuilder(title);
        if (!Util.isStringEmpty(url)) {
            try {
                URL aUrl = new URL(url);
                SpannableStringBuilder urlSpan = new SpannableStringBuilder(
                        context.getString(R.string.topstories_format_url, aUrl.getHost()));
                urlSpan = setForegroundColor(urlSpan, 0, urlSpan.length(),
                        ContextCompat.getColor(context, R.color.secondary_text));
                urlSpan = setFontSize(urlSpan, 0, urlSpan.length(), 12);
                titleSpan.append(" ").append(urlSpan);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }

        return titleSpan;
    }

    public static String formatToRelativeDate(long time, TimeUnit timeUnit, String dateFormat) {
        final int roundingOffset = 1;
        final long postTimeMillis = timeUnit.toMillis(time);
        final long timeDiffMillis = sClock.getCurrentTimeMillis() - postTimeMillis;
        long convertedTimeDiff;
        if (TimeUnit.MILLISECONDS.toSeconds(timeDiffMillis) < TOTAL_SECONDS_IN_MINUTE) {
            return "just now";
        } else if ((convertedTimeDiff = TimeUnit.MILLISECONDS.toMinutes(timeDiffMillis))
                < TOTAL_MINUTES_IN_HOUR) {
            return convertedTimeDiff + " minutes ago";
        } else if ((convertedTimeDiff = TimeUnit.MILLISECONDS.toHours(timeDiffMillis)
                + roundingOffset) < TOTAL_HOUR_IN_DAY) {
            return convertedTimeDiff + " hours ago";
        } else if ((convertedTimeDiff = TimeUnit.MILLISECONDS.toDays(timeDiffMillis)
                + roundingOffset) <= TOTAL_DAYS_IN_MONTH) {
            return convertedTimeDiff + " days ago";
        } else {
            final SimpleDateFormat formatter = new SimpleDateFormat(dateFormat,
                    Locale.getDefault());
            return formatter.format(new Date(postTimeMillis));
        }
    }

    public static SpannableStringBuilder setForegroundColor(SpannableStringBuilder str, int start,
                                                            int length, int color) {
        str.setSpan(new ForegroundColorSpan(color), start, start + length,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        return str;
    }

    public static SpannableStringBuilder setFontSize(SpannableStringBuilder str, int start,
                                                     int length, int size) {
        str.setSpan(new AbsoluteSizeSpan(size, true), start, start + length,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        return str;
    }

    public static CharSequence trimWhiteLines(CharSequence text) {

        while (text.charAt(text.length() - 1) == '\n') {
            text = text.subSequence(0, text.length() - 1);
        }
        return text;
    }

    public interface Clock {

        long getCurrentTimeMillis();
    }
}
