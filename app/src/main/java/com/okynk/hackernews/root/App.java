package com.okynk.hackernews.root;


import android.app.Application;

import com.okynk.hackernews.BuildConfig;
import com.okynk.hackernews.api.ApiModule;
import com.okynk.hackernews.features.storydetail.StoryDetailModule;
import com.okynk.hackernews.features.topstories.TopStoriesModule;

import timber.log.Timber;

public class App extends Application {

    private ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }

        mApplicationComponent = DaggerApplicationComponent.builder().applicationModule(
                new ApplicationModule(this)).apiModule(new ApiModule()).topStoriesModule(
                new TopStoriesModule()).storyDetailModule(new StoryDetailModule()).build();

    }

    public ApplicationComponent getApplicationComponent() {
        return mApplicationComponent;
    }
}
