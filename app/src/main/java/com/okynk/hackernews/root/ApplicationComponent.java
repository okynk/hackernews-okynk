package com.okynk.hackernews.root;

import com.okynk.hackernews.api.ApiModule;
import com.okynk.hackernews.features.storydetail.StoryDetailActivity;
import com.okynk.hackernews.features.storydetail.StoryDetailModule;
import com.okynk.hackernews.features.topstories.TopStoriesActivity;
import com.okynk.hackernews.features.topstories.TopStoriesModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {
        ApplicationModule.class, ApiModule.class, TopStoriesModule.class, StoryDetailModule.class
})
public interface ApplicationComponent {

    void inject(TopStoriesActivity target);

    void inject(StoryDetailActivity target);
}
