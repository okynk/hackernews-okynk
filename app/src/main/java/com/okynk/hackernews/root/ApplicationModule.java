package com.okynk.hackernews.root;

import android.app.Application;
import android.content.Context;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

@Module
public class ApplicationModule {

    private Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    @Singleton
    public Context provideContext() {
        return mApplication;
    }

    @Provides
    @Singleton
    @Named("subscribeOn")
    public Scheduler provideSubscribeOn() {
        return Schedulers.io();
    }

    @Provides
    @Singleton
    @Named("observeOn")
    public Scheduler provideObserveOn() {
        return AndroidSchedulers.mainThread();
    }

}
