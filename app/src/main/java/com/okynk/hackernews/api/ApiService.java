package com.okynk.hackernews.api;


import com.okynk.hackernews.api.model.StoryResponse;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ApiService {

    @GET(ConstantUrl.GET_TOP_STORIES)
    Observable<List<Long>> getTopStories();

    @GET(ConstantUrl.GET_STORY)
    Observable<StoryResponse> getStory(@Path("storyId") long storyId);
}
