package com.okynk.hackernews.api.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class StoryResponse {

    @SerializedName("by")
    @Expose
    private String mBy;

    @SerializedName("descendants")
    @Expose
    private long mDescendants;

    @SerializedName("id")
    @Expose
    private long mId;

    @SerializedName("kids")
    @Expose
    private List<Long> mKids;

    @SerializedName("parent")
    @Expose
    private long mParent;

    @SerializedName("score")
    @Expose
    private int mScore;

    @SerializedName("time")
    @Expose
    private long mTime;

    @SerializedName("title")
    @Expose
    private String mTitle;

    @SerializedName("type")
    @Expose
    private String mType;

    @SerializedName("url")
    @Expose
    private String mUrl;

    @SerializedName("text")
    @Expose
    private String mText;

    @Override
    public int hashCode() {
        int result = mBy != null ? mBy.hashCode() : 0;
        result = 31 * result + (int) (mDescendants ^ (mDescendants >>> 32));
        result = 31 * result + (int) (mId ^ (mId >>> 32));
        result = 31 * result + (mKids != null ? mKids.hashCode() : 0);
        result = 31 * result + (int) (mParent ^ (mParent >>> 32));
        result = 31 * result + mScore;
        result = 31 * result + (int) (mTime ^ (mTime >>> 32));
        result = 31 * result + (mTitle != null ? mTitle.hashCode() : 0);
        result = 31 * result + (mType != null ? mType.hashCode() : 0);
        result = 31 * result + (mUrl != null ? mUrl.hashCode() : 0);
        result = 31 * result + (mText != null ? mText.hashCode() : 0);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StoryResponse that = (StoryResponse) o;

        if (mDescendants != that.mDescendants) return false;
        if (mId != that.mId) return false;
        if (mParent != that.mParent) return false;
        if (mScore != that.mScore) return false;
        if (mTime != that.mTime) return false;
        if (mBy != null ? !mBy.equals(that.mBy) : that.mBy != null) return false;
        if (mKids != null ? !mKids.equals(that.mKids) : that.mKids != null) return false;
        if (mTitle != null ? !mTitle.equals(that.mTitle) : that.mTitle != null) return false;
        if (mType != null ? !mType.equals(that.mType) : that.mType != null) return false;
        if (mUrl != null ? !mUrl.equals(that.mUrl) : that.mUrl != null) return false;
        return mText != null ? mText.equals(that.mText) : that.mText == null;

    }

    public String getBy() {
        return mBy;
    }

    public void setBy(String by) {
        mBy = by;
    }

    public long getDescendants() {
        return mDescendants;
    }

    public void setDescendants(long descendants) {
        mDescendants = descendants;
    }

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        mId = id;
    }

    public List<Long> getKids() {
        return mKids;
    }

    public void setKids(List<Long> kids) {
        mKids = kids;
    }

    public long getParent() {
        return mParent;
    }

    public void setParent(long parent) {
        mParent = parent;
    }

    public int getScore() {
        return mScore;
    }

    public void setScore(int score) {
        mScore = score;
    }

    public long getTime() {
        return mTime;
    }

    public void setTime(long time) {
        mTime = time;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    public String getText() {
        return mText;
    }

    public void setText(String text) {
        mText = text;
    }
}
