package com.okynk.hackernews.api;

import com.okynk.hackernews.BuildConfig;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ApiModule {

    @Provides
    public OkHttpClient provideClient() {
        OkHttpClient.Builder okBuilder = new OkHttpClient.Builder();

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor httpLogging = new HttpLoggingInterceptor();
            httpLogging.setLevel(HttpLoggingInterceptor.Level.BODY);
            okBuilder.addInterceptor(httpLogging);
        }

        return okBuilder.build();
    }

    @Provides
    public Retrofit provideRetrofit(String baseURL, OkHttpClient client) {
        return new Retrofit.Builder().baseUrl(baseURL).client(client).addCallAdapterFactory(
                RxJava2CallAdapterFactory.create()).addConverterFactory(
                GsonConverterFactory.create()).build();
    }

    @Provides
    public ApiService provideApiService() {
        return provideRetrofit(ConstantUrl.BASE_API_URL, provideClient()).create(ApiService.class);
    }

}
