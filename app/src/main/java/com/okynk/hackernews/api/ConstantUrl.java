package com.okynk.hackernews.api;


public class ConstantUrl {

    public static final String BASE_API_URL = "https://hacker-news.firebaseio.com/v0/";
    public static final String GET_TOP_STORIES = "topstories.json";
    public static final String GET_STORY = "item/{storyId}.json";

    private ConstantUrl() {}

}
